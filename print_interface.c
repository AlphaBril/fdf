/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_interface.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 19:22:03 by edjubert          #+#    #+#             */
/*   Updated: 2018/12/19 15:52:51 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	print_header(char *img_string, int color)
{
	int	x;
	int	y;
	int	r;
	int	g;
	int b;

	y = 0;
	r = color / 65536;
	g = (color / 256) % 256;
	b = color % 256;
	while (y < 40)
	{
		x = 0;
		while (x < 2560)
		{
			img_string[((x * 4) + 4 * 2560 * y) + 0] = r;
			img_string[((x * 4) + 4 * 2560 * y) + 1] = g;
			img_string[((x * 4) + 4 * 2560 * y) + 2] = b;
			x++;
		}
		y++;
	}
}

void	print_footer(char *img_string, int color)
{
	int	x;
	int	y;
	int	r;
	int	g;
	int b;

	y = 1345;
	r = color / 65536;
	g = (color / 256) % 256;
	b = color % 256;
	while (y < 1395)
	{
		x = 0;
		while (x < 2560)
		{
			img_string[((x * 4) + 4 * 2560 * y) + 0] = r;
			img_string[((x * 4) + 4 * 2560 * y) + 1] = g;
			img_string[((x * 4) + 4 * 2560 * y) + 2] = b;
			x++;
		}
		y++;
	}
}

void	ft_interface(void **param)
{
	int		x[2];
	int		y[2];
	int		bbp[3];

	x[0] = 0;
	x[1] = 0;
	y[0] = 0;
	y[1] = 0;
	param[12] = mlx_new_image(param[0], 100, 100);
	param[13] = mlx_get_data_addr(param[12], &bbp[0], &bbp[1], &bbp[2]);
	param[14] = mlx_xpm_file_to_image(param[0], "interface.xpm", x, y);
	param[15] = mlx_new_image(param[0], 10, 10);
	param[16] = mlx_get_data_addr(param[15], &bbp[0], &bbp[1], &bbp[2]);
	param[17] = mlx_xpm_file_to_image(param[0], "fdf.xpm", x, y);
}
