/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:52 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 16:58:38 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

static char	*ft_itoa_base_calc(unsigned int tmp, int base, int power, int neg)
{
	char *ret;
	char *bas;

	bas = "0123456789ABCDEF";
	if (!(ret = malloc(sizeof(char) * (power + neg + 1))))
		return (NULL);
	if (neg == -1)
		ret[0] = '-';
	if (neg == -1)
		ret[power + 1] = '\0';
	else
		ret[power] = '\0';
	while (tmp > 0)
	{
		if (neg == -1)
			ret[power] = bas[tmp % base];
		else
			ret[power - 1] = bas[tmp % base];
		tmp = tmp / base;
		power--;
	}
	return (ret);
}

char		*ft_itoa_base(int value, int base)
{
	unsigned int	tmp;
	int				neg;
	int				power;

	neg = 1;
	power = 0;
	if (value < 0)
		neg = -1;
	tmp = value * neg;
	while (tmp > 0)
	{
		tmp = tmp / base;
		power++;
	}
	tmp = value * neg;
	if (base != 10)
		neg = 1;
	if (tmp == 0)
		return ("0\0");
	return (ft_itoa_base_calc(tmp, base, power, neg));
}
