/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:54 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 17:08:59 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		length;
	char	*rahc;

	if (s && f)
	{
		length = ft_strlen(s);
		if (!(rahc = (char*)malloc(sizeof(char) * length + 1)))
			return (NULL);
		length = 0;
		while (s[length])
		{
			rahc[length] = (*f)(s[length]);
			length++;
		}
		rahc[length] = '\0';
		return (rahc);
	}
	return (NULL);
}
