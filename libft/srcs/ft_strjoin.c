/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:53 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 17:08:08 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		i;
	int		j;
	int		length;
	char	*rahc;

	if (s1 && s2)
	{
		i = -1;
		j = 0;
		length = ft_strlen(s1) + ft_strlen(s2);
		if (!(rahc = (char*)malloc(sizeof(char) * length + 1)))
			return (NULL);
		while (s1[++i] != '\0')
			rahc[i] = s1[i];
		while (s2[j] != '\0')
		{
			rahc[i] = s2[j];
			i++;
			j++;
		}
		rahc[i] = '\0';
		return (rahc);
	}
	return (NULL);
}
