/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swallow.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 18:44:11 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 18:46:27 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

char	*ft_swallow(int fd)
{
	int		rd;
	char	*ret;
	char	buf[BUFF_SIZE + 1];

	rd = 1;
	while (rd != 0)
	{
		if ((rd = read(fd, buf, BUFF_SIZE)))
			return (NULL);
		buf[rd] = '\0';
		if (!(ret = ft_strjoin_free(ret, buf, 1)))
			return (NULL);
	}
	return (ret);
}
