/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:54 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/17 17:09:52 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

int	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	int ret;

	ret = 0;
	if (!s1 || !s2)
		return (ret);
	ret = ft_strncmp(s1, s2, n);
	if (ret == 0)
		return (1);
	else
		return (0);
}
