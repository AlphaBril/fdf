/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 14:01:57 by fldoucet          #+#    #+#             */
/*   Updated: 2018/12/19 16:17:11 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "./libft/include/libft.h"
# include <math.h>
# include "mlx.h"
# include <fcntl.h>

typedef struct		s_fdf
{
	int				x;
	int				y;
	int				z;
	int				d_z;
	struct s_fdf	*next;
}					t_fdf;

void				ft_error(int error_type);
int					ft_parse(int fd, t_fdf **map, int verif);
void				fdf(t_fdf **map, int size);
void				ft_draw_iso(void **param, int *size);
void				ft_draw_par(void **param, int *size);
int					dy_eq_zero(void *param[12], int x[2], int y[2], int color);
int					dx_eq_zero(void *param[12], int x[2], int y[2], int color);
int					try_to_draw(void *param[12], int x[2], int y[2], int color);
int					dx_and_dy_greater(
							void *param[12], int x[2], int y[2], int color);
int					dx_lower_than_zero(
							void *param[12], int x[2], int y[2], int color);
int					dx_greater_than_zero(
							void *param[12], int x[2], int y[2], int color);
int					minus_dx_greater_and_eq_dy(
							void *param[12], int x[2], int y[2], int color);
int					minus_dx_lower_than_dy(
							void *param[12], int x[2], int y[2], int color);
int					dx_and_dy_lower_than_zero(
							void *param[12], int x[2], int y[2], int color);
void				fill_pixel(char *img_string, int x, int y, int color);
void				clear_pixels(char *img_string);
void				ft_interface(void **param);
void				print_header(char *img_string, int color);
void				print_footer(char *img_string, int color);

#endif
