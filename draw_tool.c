/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_tool.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/15 15:01:55 by fldoucet          #+#    #+#             */
/*   Updated: 2019/03/22 18:53:18 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static double	ft_itor(int nbr)
{
	double ret;
	double pi;

	pi = 3.1416;
	ret = nbr;
	ret = ret * pi;
	ret = ret / 180;
	return (ret);
}

static void		ft_calc_l(void **param, t_fdf *tmp, int (*lx)[2], int (*ly)[2])
{
	int *x;
	int *y;

	x = *lx;
	y = *ly;
	x[0] = (tmp->x - tmp->y) * cos(ft_itor((int)param[9]))
			* (int)param[2] + 1280 + (int)param[6];
	y[0] = -((tmp->z / (int)param[7]) * (int)param[2])
			+ (tmp->x + tmp->y) * sin(ft_itor((int)param[9]))
				* (int)param[2] + (698 - (*(int*)param[10]
				* (int)param[2]) / 2) + (int)param[5];
	x[1] = (tmp->x - (tmp->y + 1)) * cos(ft_itor((int)param[9]))
			* (int)param[2] + 1280 + (int)param[6];
	y[1] = -((tmp->d_z / (int)param[7]) * (int)param[2])
			+ (tmp->x + (tmp->y + 1)) * sin(ft_itor((int)param[9]))
				* (int)param[2] + (698 - (*(int*)param[10]
				* (int)param[2]) / 2) + (int)param[5];
}

static void		ft_calc_r(void **param, t_fdf *tmp, int (*lx)[2], int (*ly)[2])
{
	int *x;
	int *y;

	x = *lx;
	y = *ly;
	x[0] = (tmp->x - tmp->y) * cos(ft_itor((int)param[9]))
			* (int)param[2] + 1280 + (int)param[6];
	y[0] = -((tmp->z / (int)param[7]) * (int)param[2])
			+ ((tmp->x + tmp->y) * sin(ft_itor((int)param[9]))
				* (int)param[2]) + (698 - (*(int*)param[10]
				* (int)param[2]) / 2) + (int)param[5];
	x[1] = ((tmp->x + 1) - tmp->y) * cos(ft_itor((int)param[9]))
			* (int)param[2] + 1280 + (int)param[6];
	y[1] = -((tmp->next->z / (int)param[7]) * (int)param[2])
			+ ((tmp->x + 1) + tmp->y) * sin(ft_itor((int)param[9]))
				* (int)param[2] + (698 - (*(int*)param[10]
				* (int)param[2]) / 2) + (int)param[5];
}

static	void	ft_caca(void **param)
{
	print_header(param[11], (int)param[3]);
	print_footer(param[11], (int)param[3]);
	mlx_put_image_to_window(param[0], param[1], param[8], 0, 0);
	mlx_put_image_to_window(param[0], param[1], param[14], 2300, 50);
	mlx_put_image_to_window(param[0], param[1], param[17], 2560 / 2, 5);
}

void			ft_draw_iso(void **param, int *size)
{
	int		x[2];
	int		y[2];
	t_fdf	**map;
	t_fdf	*tmp;

	mlx_clear_window(param[0], param[1]);
	clear_pixels(param[11]);
	map = param[4];
	tmp = *map;
	while (tmp->next)
	{
		if (tmp->next->x != 0)
		{
			ft_calc_r(param, tmp, &x, &y);
			try_to_draw(param, x, y, (int)param[3]);
		}
		if (tmp->y != *size)
		{
			ft_calc_l(param, tmp, &x, &y);
			try_to_draw(param, x, y, (int)param[3]);
		}
		tmp = tmp->next;
	}
	ft_caca(param);
}
