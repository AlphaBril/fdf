/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 18:34:26 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/25 16:27:09 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

/*
** Listage des touches utiles
** esc : 53 | up : 126 | down : 125 | left : 123 | right : 124 | + : 69 |
** - : 78 | ctrl : 256 | shift : 257 | 1 : 18 | 2 : 19 | 3 : 20 | y : 16
*/

static	void	ft_zoom(void ***param, int key)
{
	void **tmp;

	tmp = *param;
	if (key == 69)
		tmp[2] = tmp[2] + 5;
	if (key == 78)
		if ((int)tmp[2] > 5)
			tmp[2] = tmp[2] - 5;
}

static	void	ft_move(void ***param, int key)
{
	void **tmp;

	tmp = *param;
	if (key == 126)
		tmp[5] = tmp[5] - 10;
	if (key == 125)
		tmp[5] = tmp[5] + 10;
	if (key == 123)
		tmp[6] = tmp[6] - 10;
	if (key == 124)
		tmp[6] = tmp[6] + 10;
}

static	void	ft_color(void ***param, int key)
{
	void **tmp;

	tmp = *param;
	if (key == 18)
		tmp[3] = (void*)0x00FF00;
	if (key == 19)
		tmp[3] = (void*)0xFF0000;
	if (key == 20)
		tmp[3] = (void*)0x0000FF;
	if (key == 21)
		tmp[3] = (void*)0xFFFFFF;
}

static	int		deal_key(int key, void **param)
{
	if (key == 53)
		exit(0);
	if (key == 69 || key == 78)
		ft_zoom(&param, key);
	if (key == 18 || key == 19 || key == 20 || key == 21)
		ft_color(&param, key);
	if (key == 126 || key == 125 || key == 123 || key == 124)
		ft_move(&param, key);
	if (key == 256)
		param[7] = param[7] + 1;
	if (key == 257)
		if ((int)param[7] > 1)
			param[7] = param[7] - 1;
	if (key == 12)
		param[9] = param[9] + 10;
	if (key == 14)
		param[9] = param[9] - 10;
	if (key == 16)
		param[12] = param[12] + 1;
	if ((int)param[12] % 2 == 0)
		ft_draw_iso(param, param[10]);
	else
		ft_draw_par(param, param[10]);
	return (0);
}

void			fdf(t_fdf **map, int size)
{
	int		bbp;
	int		s_l;
	int		endian;
	void	*param[18];

	param[0] = mlx_init();
	param[1] = mlx_new_window(param[0], 2560, 1395
			, "=== fldoucet-edjubert Fil de Fer ===");
	if (size < 20)
		param[2] = (void *)50;
	else
		param[2] = (void *)10;
	param[3] = (void *)0xFFFFFF;
	param[4] = map;
	param[5] = (void *)0;
	param[6] = (void *)0;
	param[7] = (void *)1;
	param[8] = mlx_new_image(param[0], 2560, 1395);
	param[9] = (void *)30;
	param[10] = &size;
	param[11] = mlx_get_data_addr(param[8], &bbp, &s_l, &endian);
	ft_interface(param);
	ft_draw_iso(param, param[10]);
	mlx_hook(param[1], 2, 1L << 0, deal_key, param);
	mlx_loop(param[0]);
}
