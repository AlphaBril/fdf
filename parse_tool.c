/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_tool.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 15:31:31 by fldoucet          #+#    #+#             */
/*   Updated: 2018/12/19 16:19:51 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static	void	ft_strdigit(char *str)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (str[i])
	{
		if (ft_isdigit(str[i]) == 1)
			j++;
		i++;
	}
	if (j == 0)
		ft_error(3);
}

static	void	ft_underz(t_fdf **map)
{
	t_fdf	*l1;
	t_fdf	*l2;

	l1 = *map;
	if (l1 && l1->next)
	{
		l2 = l1->next;
		while (l2->x != 0)
			l2 = l2->next;
		while (l2)
		{
			l1->d_z = l2->z;
			l1 = l1->next;
			l2 = l2->next;
		}
	}
	else
		ft_error(3);
}

static	void	ft_lstpush(t_fdf **map, t_fdf *new)
{
	t_fdf *tmp;

	tmp = *map;
	if (!tmp)
		*map = new;
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
}

static	int		ft_trans(t_fdf **map, char *str, int y)
{
	char	**temp;
	t_fdf	*tmp;
	int		i;
	int		size;

	i = 0;
	size = ft_countword(str, 0, 0, ' ');
	temp = ft_strsplit(str, ' ');
	while (i < size)
	{
		if (!(tmp = (t_fdf *)malloc(sizeof(t_fdf))))
			ft_error(4);
		ft_strdigit(temp[i]);
		tmp->x = i;
		tmp->y = y;
		tmp->z = ft_atoi(temp[i]);
		tmp->d_z = 0;
		tmp->next = NULL;
		ft_lstpush(map, tmp);
		free(temp[i]);
		i++;
	}
	free(temp);
	return (0);
}

int				ft_parse(int fd, t_fdf **map, int verif)
{
	char	*line;
	int		ret;
	int		y;

	y = 0;
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		if (ft_countword(line, 0, 0, ' ') != verif)
			ft_error(3);
		ft_trans(map, line, y);
		free(line);
		y++;
	}
	if (ret == -1)
		return (-1);
	ft_underz(map);
	return (y);
}
