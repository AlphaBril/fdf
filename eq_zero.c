/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eq_zero.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 15:40:30 by edjubert          #+#    #+#             */
/*   Updated: 2018/12/18 20:17:34 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	dx_eq_zero(void *param[12], int x[2], int y[2], int color)
{
	if ((y[1] - y[0]) != 0)
	{
		if (y[1] - y[0] > 0)
		{
			while (y[0] < y[1])
			{
				fill_pixel(param[11], x[0], y[0], color);
				y[0]++;
			}
		}
		else
		{
			while (y[0] > y[1])
			{
				fill_pixel(param[11], x[0], y[0], color);
				y[0]--;
			}
		}
	}
	return (0);
}

int	dy_eq_zero(void *param[12], int x[2], int y[2], int color)
{
	if (x[1] > x[0])
	{
		while (x[0] < x[1])
		{
			fill_pixel(param[11], x[0], y[0], color);
			x[0]++;
		}
	}
	else if (x[1] < x[0])
	{
		while (x[0] > x[1])
		{
			fill_pixel(param[11], x[0], y[0], color);
			x[0]--;
		}
	}
	return (0);
}
