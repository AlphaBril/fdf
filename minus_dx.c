/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minus_dx.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 15:44:02 by edjubert          #+#    #+#             */
/*   Updated: 2018/12/18 20:17:58 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	minus_dx_greater_and_eq_dy(void *param[12], int x[2], int y[2], int color)
{
	int	e;
	int	dx;
	int	dy;

	dx = x[1] - x[0];
	dy = y[1] - y[0];
	e = dx;
	dx *= 2;
	dy *= 2;
	while (x[0] > x[1])
	{
		fill_pixel(param[11], x[0], y[0], color);
		x[0]--;
		if ((e += dy) >= 0)
		{
			y[0]++;
			e += dx;
		}
	}
	return (0);
}

int	minus_dx_lower_than_dy(void *param[12], int x[2], int y[2], int color)
{
	int	e;
	int	dx;
	int	dy;

	dx = x[1] - x[0];
	dy = y[1] - y[0];
	e = dy;
	dy *= 2;
	dx *= 2;
	while (y[0] < y[1])
	{
		fill_pixel(param[11], x[0], y[0], color);
		y[0]++;
		if ((e += dx) <= 0)
		{
			x[0]--;
			e += dy;
		}
	}
	return (0);
}
