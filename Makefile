# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/13 12:07:06 by fldoucet          #+#    #+#              #
#    Updated: 2019/03/22 17:43:52 by fldoucet         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=fdf
CSOURCE=./main.c \
		./parse_tool.c \
		./fdf.c \
		./draw_tool.c \
		./draw_tool_par.c \
		./draw_between_lines.c \
		./dx_and_dy_greater.c \
		./dx_and_dy_lower_than_zero.c \
		./dx_greater_than_zero.c \
		./dx_lower_than_zero.c \
		./eq_zero.c \
		./minus_dx.c \
		./print_interface.c
HSOURCE=./includes/fdf.h
OSOURCE=$(CSOURCE:.c=.o)
LIBFTFOLDER=./libft/
INCXFOLDER=/usr/local/include/
LIBXFOLDER=/usr/local/lib/
LIBFT=./libft/libft.a
CFLAGS=-Wall -Wextra -Werror
LIBXFLAGS=-lmlx -framework OpenGL -framework AppKit
CC=gcc

all: $(NAME)

$(NAME): $(OSOURCE)
	$(CC) $(CFLAGS) -o $(NAME) $(OSOURCE) $(LIBFT) -L $(LIBXFOLDER) $(LIBXFLAGS)

$(OSOURCE):
	make -C $(LIBFTFOLDER) all
	$(CC) $(CFLAGS) -I $(INCXFOLDER) -c $(CSOURCE)

clean:
	make -C $(LIBFTFOLDER) clean
	/bin/rm -rf $(OSOURCE)

fclean: clean
	/bin/rm -rf $(LIBFT)
	/bin/rm -rf $(NAME)

re: fclean all

.PHONY: clean
