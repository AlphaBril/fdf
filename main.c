/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 12:01:34 by fldoucet          #+#    #+#             */
/*   Updated: 2018/12/19 16:35:39 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

/*
** Listage des touches utiles
** esc : 53 | up : 126 | down : 125 | left : 123 | right : 124 | + : 69 |
** - : 78 | ctrl : 256 | shift : 257 | 1 : 18 | 2 : 19 | 3 : 20 | y : 16
*/

void	ft_error(int error_type)
{
	if (error_type == 0)
		ft_putendl("usage: fdf file");
	if (error_type == 1)
		ft_putendl("error opening file");
	if (error_type == 2)
		ft_putendl("error reading file");
	if (error_type == 3)
		ft_putendl("corrupted map");
	if (error_type == 4)
		ft_putendl("malloc error");
	if (error_type == 5)
		ft_putendl("close error");
	exit(0);
}

int		main(int ac, char **av)
{
	t_fdf	*map;
	char	*verif;
	int		fd;
	int		ret;

	if (ac != 2)
		ft_error(0);
	if ((fd = open(av[1], O_RDONLY)) == -1)
		ft_error(1);
	if (get_next_line(fd, &verif) == -1)
		ft_error(2);
	if ((fd = open(av[1], O_RDONLY)) == -1)
		ft_error(1);
	if ((ret = ft_parse(fd, &map, ft_countword(verif, 0, 0, ' '))) == -1)
		ft_error(2);
	fdf(&map, (ret - 1));
	close(fd);
	return (0);
}
