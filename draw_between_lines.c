/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_between_lines.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 15:31:34 by edjubert          #+#    #+#             */
/*   Updated: 2019/03/22 18:58:31 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		fill_pixel(char *img_string, int x, int y, int color)
{
	int	r;
	int	g;
	int b;

	r = color / 65536;
	g = (color / 256) % 256;
	b = color % 256;
	if ((((x * 4) + 4 * 2560 * y) + 3) < (2560 * 1395 * 4) && x > 0
			&& y > 0 && x < 2560 && y < 1395)
	{
		img_string[((x * 4) + 4 * 2560 * y) + 0] = r;
		img_string[((x * 4) + 4 * 2560 * y) + 1] = g;
		img_string[((x * 4) + 4 * 2560 * y) + 2] = b;
	}
}

void		clear_pixels(char *img_string)
{
	int	i;

	i = 0;
	while (i < (2560 * 1395 * 4))
	{
		img_string[i] = 0;
		i++;
	}
}

int			try_to_draw(void *param[12], int x[2], int y[2], int color)
{
	int		dx;

	if ((dx = x[1] - x[0]) != 0)
	{
		if (dx > 0)
			dx_greater_than_zero(param, x, y, color);
		else
			dx_lower_than_zero(param, x, y, color);
	}
	else
		dx_eq_zero(param, x, y, color);
	fill_pixel(param[11], x[1], y[1], 0x8dbe49);
	return (0);
}
