/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dx_greater_than_zero.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 15:36:42 by edjubert          #+#    #+#             */
/*   Updated: 2018/12/18 20:17:06 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	dx_greater_than_minus_dy(
	void *param[12], int x[2], int y[2], int color)
{
	int	e;
	int	dx;
	int	dy;

	dx = x[1] - x[0];
	dy = y[1] - y[0];
	e = dx;
	dx *= 2;
	dy *= 2;
	while (x[0] < x[1])
	{
		fill_pixel(param[11], x[0], y[0], color);
		x[0]++;
		if ((e += dy) < 0)
		{
			y[0]--;
			e += dx;
		}
	}
	return (0);
}

static int	dx_lower_and_eq_than_minus_dy(
	void *param[12], int x[2], int y[2], int color)
{
	int	e;
	int	dx;
	int	dy;

	dx = x[1] - x[0];
	dy = y[1] - y[0];
	e = dy;
	dy *= 2;
	dx *= 2;
	while (y[0] > y[1])
	{
		fill_pixel(param[11], x[0], y[0], color);
		y[0]--;
		if ((e += dx) > 0)
		{
			x[0]++;
			e += dy;
		}
	}
	return (0);
}

static int	dx_greater_and_dy_lower(
	void *param[12], int x[2], int y[2], int color)
{
	if ((x[1] - x[0]) > -(y[1] - y[0]))
		dx_greater_than_minus_dy(param, x, y, color);
	else
		dx_lower_and_eq_than_minus_dy(param, x, y, color);
	return (0);
}

int			dx_greater_than_zero(void *param[12], int x[2], int y[2], int color)
{
	if ((y[1] - y[0]) != 0)
	{
		if (y[1] - y[0] > 0)
			dx_and_dy_greater(param, x, y, color);
		else
			dx_greater_and_dy_lower(param, x, y, color);
	}
	else
		dy_eq_zero(param, x, y, color);
	return (0);
}
