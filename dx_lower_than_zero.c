/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dx_lower_than_zero.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 15:38:01 by edjubert          #+#    #+#             */
/*   Updated: 2018/12/18 20:17:20 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	dx_lower_and_dy_greater(
	void *param[12], int x[2], int y[2], int color)
{
	if (-(x[1] - x[0]) >= y[1] - y[0])
		minus_dx_greater_and_eq_dy(param, x, y, color);
	else
		minus_dx_lower_than_dy(param, x, y, color);
	return (0);
}

int			dx_lower_than_zero(void *param[12], int x[2], int y[2], int color)
{
	if ((y[1] - y[0]) != 0)
	{
		if (y[1] - y[0] > 0)
			dx_lower_and_dy_greater(param, x, y, color);
		else
			dx_and_dy_lower_than_zero(param, x, y, color);
	}
	else
		dy_eq_zero(param, x, y, color);
	return (0);
}
